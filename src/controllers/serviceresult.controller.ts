// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/context';
import {Request, RestBindings, get, ResponseObject} from '@loopback/rest';
import {inject} from '@loopback/context';

/**
 * OpenAPI response for ping()
 */
const SERVICE_RESULT: ResponseObject = {
  description: 'Ping Response',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          title: {type: 'string'},
          date: {type: 'string'},
          headers: {
            type: 'object',
            properties: {
              'Content-Type': {type: 'string'},
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};

export class ServiceresultController {
  constructor(@inject(RestBindings.Http.REQUEST) private req: Request) {
  }

  @get('/save_service_data', {
    responses: {
      '200': SERVICE_RESULT,
    },
  })
  hello(): object {
    console.log(this.req);
    return {
      title: "Saved",
      date: new Date(),
      headers: Object.assign({}, this.req.headers)
    };
  }
}
